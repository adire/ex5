<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Status extends Model
{
    public function candidatesStatus(){ 
        return $this->hasMany('App\Candidate'); // each status has many candidates
    }

    public static function next($status_id){ //We want to get only the statuses that we can trasfer from the current status
        $nextstages = DB::table('nextstages')->where('from', $status_id)->pluck('to'); //Query : get from function status_id, in nextstages table we get only the 'to' that we can get from the specific 'from'
        return self::find($nextstages)->all(); //get all id's
    }

    public static function allowed($from,$to){
        $allowed = DB::table('nextstages')->where('from',$from)->where('to',$to)->get();
        if(isset($allowed)) return true;
        return false;
    }
}
