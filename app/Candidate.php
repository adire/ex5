<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $fillable = ['name','email']; // which fields I allow to update

    public function owner(){ // user is the owner of candidate
        return $this->belongsTo('App\User', 'user_id'); // each candidate has only one user (owner)
    }

    public function situation(){ 
        return $this->belongsTo('App\Status', 'status_id'); // each candidate has only one status
    }
}
