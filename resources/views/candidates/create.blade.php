@extends('layouts.app')
@section('title', 'Create Candidate')
@section('content')
<h1>Create candidate</h1>
<form method = "post" action = "{{action('CandidatesController@store')}}">
    @csrf
    <div class="form-group">
        <label for = "name">Candidate name</label>
        <input type = "text" class="form-control" name = "name" placeholder="Enter name">
    </div>
    <div class="form-group">
        <label for = "name">Candidate email</label>
        <input type = "text" class="form-control" name = "email" placeholder="Enter email">
    </div>
    <div>
        <input type = "submit" class="btn btn-primary" name = "submit" value = "Create Candidate">
    </div>
</form>     
@endsection
