<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('candidates', 'CandidatesController')->middleware('auth');

Route::get('candidates/delete/{id}', 'CandidatesController@destroy')->name('candidate.delete');

Route::get('candidates/changeuser/{cid}/{uid?}', 'CandidatesController@changeUser')->name('candidate.changeuser'); // cid is required and uid is optional

Route::get('candidates/changestatus/{cid}/{sid}', 'CandidatesController@changeStatus')->name('candidate.changestatus')->middleware('auth'); // cid is required and sid is optional

Route::get('/profile', 'CandidatesController@getCandidate')->name('candidate.getcandidate')->middleware('auth');

Route::get('/student/{id}', function($id = 'No student found'){ // Getting parameters required + name 
    return 'We got student with id '.$id;
});

Route::get('/car/{id?}', function($id = NULL){ //add ? and NULL to display scrypt if I didn't enter id
    if(isset($id)){ //בדיקה האם הכנסתי משתנה id
        //TODO: validtae for integer
        return "We got car $id";
    }
    else {
        return 'We need the id to find your id';
    }
});

Route::get('/comment/{id}', function ($id) {
    return view('comment', compact('id')); // compact is a php function to transfer variables
});

#Ex5 
Route::get('/users/{email}/{name?}', function($email, $name = 'name missing'){ 
        return view('users', compact('email','name'));
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
